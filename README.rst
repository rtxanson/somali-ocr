Somali-OCR
==========

An extension for Tesseract_ for OCR in Somali. In addition to Tesseract, it
uses word frequency data I have collected from large news and religious corpuses
to include frequent and non-frequent words to aid Tesseract's word recognition.

.. _Tesseract: http://code.google.com/p/tesseract-ocr/

Results
-------

So far, they are much better than Google's OCR in PDFs it has absorbed.::

    Fiidkii kolkii qorraxdu dhacdo ninka xooloraacatada ah ee soomaaliyeed
    xoolahiisa buu soo xereeyaa. Carruurtiisa yaryari durba ma seexdaan ee
    waxay sugaan inta xoolaha irmaani ka caweysimaan oo caano looga lisayo.
    Inta xooluhu caweynayaan carruurta waxaa loo shidaa dab ay kulaalaan,
    waxaana sheekoxariirooyin yaab leh ku maaweeliya oo lulada kareebashee-
    koyahanno caan ah, Carruurtu aad bay u jecel yihiin sheekooyinka waa-
    yeelku u tebinayo ee yaabka iyo irkigga leh. Markii ay hanoqaadaan
    carruurtaasi waxay noqon doonaan garwadeenkii bulshadooda. Inta ka
    horraysase waa in lagu carbiyo aqoonta dhaqaneed ee dadkooda ee ku
    dheehan suugaanta aan qorrayn iyo murtideeda ee dalkoodu hodanka ka
    yahay. Sidaas awgeed, tebinta sheekadu waa dugsinololeedka reer-guuraaga
    soomaaliyeed, halkaas oo ababinta iyo carbinta qofku ka unkamaan inta
    uu sebi yahay.

Not absolutely perfect yet, but getting there.

Training data
-------------

The data used so far is:

- Image data from *Shekoxariirooyin Soomaaliyeed* (Axmed Cartan Xaange), and
  manually set images for certain fonts with texts from *Sheekooyin Soomaaliyeed*
  (Musse Cumar Islaan) and also manually set images with source text for voter
  registration in Finland.

- Word data from the Somali-language Qur'an and Bible, and a set of news
  articles collected from BBC_ and SverigesRadio_, and eventually including
  output from Tesseract.

.. _SverigesRadio: http://sr.se/somaliska
.. _BBC: http://www.bbc.co.uk/somali

If you wish to contribute images to train additional fonts, it is recommended
that the source files are of very high quality, this helps Tesseract identify
individual letters better, and make distinctions between similar letters (e.g.
`l` and `1`). 

Requirements
------------

- Tesseract 3.0.2
- make

Installation
------------

You have two options, either check out the repository and build the library, or
download a premade training file. If you wish to contribute to improving the
library, you will need to check out the source.

Building: run `make` drink some coffee or tea, and then copy the resulting
file, `som.traineddata` to your `tessdata` directory. Note, the install process
will create several files, but this is the only one you need. If there are any
errors during the build process, please tell me so I can help fix them. I have
made a compiled file available,

Usage
-----

Tesseract prefers to have .TIFF files as input. These can be single pages, or
multiple pages.::

    tesseract samplefile.tiff output_header -l som

Tesseract would then save output to `output_header.txt`.


TODOs
-----

- Easier means to new input word data, should new texts alter word frequency.



