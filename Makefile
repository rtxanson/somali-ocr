
# TODO: combine these with other fonts, pattern matching

COURIER_TIFFS = data/_in.som.courier.exp0.tiff \
				data/_in.som.courier.exp1.tiff


# tesseract's current version doesn't support some things, so we need to make a
# multipage tiff.
C_MERGED = som.courier.exp0.tiff

C_BOXES = $(MERGED:data/%.tiff=%.box)

C_TRAINED = $(MERGED:data/%.tiff=%.tr)

$(C_MERGED): $(COURIER_TIFFS)
	tiffcp -c lzw $(COURIER_TIFFS) $(C_MERGED)

som.courier.exp0.box: $(C_MERGED)
	tesseract $^ $(basename $^) batch.nochop makebox

# # what is -l format for? training from existing?
# tesseract som.courier.exp0.tiff som.courier.exp0 -l som batch.nochop makebox

som.courier.exp0.tr: $(C_MERGED)
	tesseract $^ $(basename $^) nobatch box.train


# Add more corpus training data here
GENTIUM_TIFFS = data/_in.som.gentium.exp0.tiff \
			  data/_in.som.gentium.exp1.tiff \
			  data/_in.som.gentium.exp2.tiff \
			  data/_in.som.gentium.exp3.tiff \
			  data/_in.som.gentium.exp4.tiff

# tesseract's current version doesn't support some things, so we need to make a
# multipage tiff.
MERGED = som.gentium.exp0.tiff

BOXES = $(MERGED:data/%.tiff=%.box)

TRAINED = $(MERGED:data/%.tiff=%.tr)

$(MERGED): $(GENTIUM_TIFFS)
	tiffcp -c lzw $(GENTIUM_TIFFS) $(MERGED)

som.gentium.exp0.box: $(MERGED)
	tesseract $^ $(basename $^) batch.nochop makebox

# # what is -l format for? training from existing?
# tesseract som.gentium.exp0.tiff som.gentium.exp0 -l som batch.nochop makebox

som.gentium.exp0.tr: $(MERGED)
	tesseract $^ $(basename $^) nobatch box.train

som.unicharset: som.gentium.exp0.box som.courier.exp0.box
	unicharset_extractor $^
	mv unicharset som.unicharset


font_properties:
	@echo "courier 0 1 0 1 0" >> $@
	@echo "gentium 0 0 0 1 0" >> $@

som.charset: font_properties som.unicharset som.gentium.exp0.tr som.courier.exp0.tr
	mftraining -F font_properties -U som.unicharset -O som.charset som.gentium.exp0.tr som.courier.exp0.tr

som.normproto: som.gentium.exp0.tr som.courier.exp0.tr
	cntraining $^
	mv normproto som.normproto

som.freq-dawg: data/_in.freq som.unicharset
	wordlist2dawg data/_in.freq $@ som.unicharset

som.word-dawg: data/_in.words som.unicharset
	wordlist2dawg data/_in.words $@ som.unicharset

som.Microfeat som.inttemp som.font_properties:
	mv Microfeat som.Microfeat
	mv font_properties som.font_properties
	mv inttemp som.inttemp
	mv pffmtable som.pffmtable

deps: $(MERGED) \
	 $(BOXES) \
	 $(TRAINED) \
	 som.unicharset \
	 som.charset \
	 som.normproto \
	 som.freq-dawg \
	 som.word-dawg \
	 som.inttemp \
	 som.Microfeat \
	 som.traineddata

som.traineddata: deps
	combine_tessdata som.

all: som.traineddata

install: all
	cp som.traineddata /usr/local/Cellar/tesseract/3.01/share/tessdata

clean:
	rm som.*
	rm inttemp
	rm pffmtable
	rm Microfeat
	rm font_properties

